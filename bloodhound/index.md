---
Title: bloodhound
Homepage: https://github.com/BloodHoundAD/BloodHound
Repository: https://gitlab.com/kalilinux/packages/bloodhound
Architectures: amd64 arm64
Version: 4.1.1~precompiled-0kali1
Metapackages: kali-linux-everything 
Icon: images/bloodhound-logo.svg
PackagesInfo: |
 ### bloodhound
 
  This package contains BloodHound, a single page Javascript web application.
  BloodHound uses graph theory to reveal the hidden and often unintended
  relationships within an Active Directory environment. Attackers can use
  BloodHound to easily identify highly complex attack paths that would otherwise
  be impossible to quickly identify. Defenders can use BloodHound to identify and
  eliminate those same attack paths. Both blue and red teams can use BloodHound
  to easily gain a deeper understanding of privilege relationships in an Active
  Directory environment.
 
 **Installed size:** `262.71 MB`  
 **How to install:** `sudo apt install bloodhound`  
 
 {{< spoiler "Dependencies:" >}}
 * neo4j
 {{< /spoiler >}}
 
 ##### bloodhound
 
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
